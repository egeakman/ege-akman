(function () {
    var CSS = {
        arena: {
            width: 900,
            height: 600,
            background: '#62247B',
            position: 'fixed',
            top: '50%',
            left: '50%',
            zIndex: '999',
            transform: 'translate(-50%, -50%)'
        },
        ball: {
            width: 15,
            height: 15,
            position: 'absolute',
            top: 300,
            left: 443.5,
            borderRadius: 50,
            background: '#C6A62F',
            "z-index": 4,
        },
        ballTrail: {
            "background-color": '#DDC56F',
            "z-index": 3,
            opacity: 0.8
        },
        ballTrail2: {
            "background-color": "#E7D698",
            "z-index": 2,
            opacity: 0.4
        },
        ballTrail3: {
            "background-color": "#F0E6C1",
            "z-index": 1,
            opacity: 0.2
        },
        line: {
            width: 0,
            height: 600,
            borderLeft: '2px dashed #C6A62F',
            position: 'absolute',
            top: 0,
            left: '50%'
        },
        stick: {
            width: 12,
            height: 85,
            position: 'absolute',
            background: '#C6A62F'
        },
        stick1: {
            left: 0,
            top: 150
        },
        stick2: {
            right: 0,
            top: 150
        },
        stickinitial: {
            top: 150
        },
        scoreboard: {
            width: "50%",
            height: "20%",
            margin: "auto"
        },
        scoreLeft: {
            color: "yellow",
            "font-family": "sans-serif",
            "font-size": "400%",
            "margin-top": "4%",
            float: "left"
        },
        scoreRight: {
            color: "yellow",
            "font-family": "sans-serif",
            "font-size": "400%",
            "margin-top": "4%",
            float: "right"
        },
        menu: {
            width: "50%",
            margin: "auto",
            "display": "flex",
            "justify-content": "space-between",
            "align-items": "flex-end",
            height: "150%"
        },
        button: {
            "background-color": "yellow",
            width: 100,
            height: 50,
            "text-align": "center",
            borderRadius: "5%",
            color: "black",
            margin: "auto",
            "vertical-align": "center"
        }
    };

    var CONSTS = {
        gameSpeed: 20,
        score1: 0,
        score2: 0,
        stick1Speed: 0,
        stick2Speed: 0,
        ballTopSpeed: 0,
        ballLeftSpeed: 0,
        cpu1Speed: 5,
        isCpu1Active: false,
        isCpu2Active: false,
        isGameActive: false,
        ballCount: -1
    };

    function start() {
        draw();
        setEvents();
        roll();
        loop();
        getScoresFromLocalStorage();
    }

    function getScoresFromLocalStorage() {
        $('#scoreRight').text(localStorage.getItem("rightScore") || 0);
        $('#scoreLeft').text(localStorage.getItem("leftScore") || 0);
    }

    function draw() {

        $('<div/>', { id: 'pong-game' }).css(CSS.arena).appendTo('body');
        $('<div/>', { id: 'pong-line' }).css(CSS.line).appendTo('#pong-game');
        $('<div/>', { id: 'pong-ball' }).css(CSS.ball).appendTo('#pong-game');
        $('<div/>', { id: 'pong-ball-trail' }).css($.extend(CSS.ball, CSS.ballTrail)).appendTo('#pong-game');
        $('<div/>', { id: 'pong-ball-trail2' }).css($.extend(CSS.ball, CSS.ballTrail2)).appendTo('#pong-game');
        $('<div/>', { id: 'pong-ball-trail3' }).css($.extend(CSS.ball, CSS.ballTrail3)).appendTo('#pong-game');
        $('<div/>', { id: 'stick-1' }).css($.extend(CSS.stick1, CSS.stick))
            .appendTo('#pong-game');
        $('<div/>', { id: 'stick-2' }).css($.extend(CSS.stick2, CSS.stick))
            .appendTo('#pong-game');
        drawScoreboard();
        drawMenu();
    }

    function drawScoreboard() {
        $('<div/>', { id: "scoreboard" }).css(CSS.scoreboard).appendTo('#pong-game');
        $('<div/>', { id: "scoreLeft" }).css(CSS.scoreLeft).appendTo('#scoreboard');
        $('<div/>', { id: "scoreRight" }).css(CSS.scoreRight).appendTo('#scoreboard');
        $('#scoreLeft').text('0');
        $('#scoreRight').text('0');
    }

    function drawMenu() {
        $('<div/>', { id: 'menu' }).css(CSS.menu).appendTo('#pong-game');
        $('<div/>', { id: '2-Players' }).css(CSS.button).text("2 Players").appendTo('#menu');
        $('<div/>', { id: 'Player-vs-CPU' }).css(CSS.button).text("Player vs CPU").appendTo('#menu');
        $('<div/>', { id: 'CPU-vs-CPU' }).css(CSS.button).text("CPU vs CPU").appendTo('#menu');
        $('<div/>', { id: 'reset' }).css(CSS.button).text("Reset").appendTo('#menu');

    }

    function bindToMenuButtonClicks() {
        $('#2-Players').on('click', function (event) {
            CONSTS.isGameActive = true;
            setButtonStyle('#2-Players', "#Player-vs-CPU", "#CPU-vs-CPU");
        });
        $('#Player-vs-CPU').on('click', function (event) {
            CONSTS.isCpu1Active = true;
            CONSTS.isGameActive = true;
            setButtonStyle('#Player-vs-CPU', "#2-Players", "#CPU-vs-CPU");
        });
        $('#CPU-vs-CPU').on('click', function (event) {
            CONSTS.isCpu1Active = true;
            CONSTS.isCpu2Active = true;
            CONSTS.isGameActive = true;
            setButtonStyle('#CPU-vs-CPU', "#Player-vs-CPU", "#2-Players");
        });
        $('#reset').on('click', function (event) {
            CONSTS.isGameActive = false;
            CONSTS.isCpu1Active = false;
            CONSTS.isCpu2Active = false;
            reset();
            saveScoresToLocalStorage(0, 0);
            resetBallLocation();
            resetButtonStyle();
        });
    }

    function saveScoresToLocalStorage(leftScore, rightScore) {
        localStorage.setItem("leftScore", leftScore);
        localStorage.setItem("rightScore", rightScore);
    }

    function setButtonStyle(activeButton, passive1, passive2) {
        $(activeButton).css({ "opacity": "0.8", "color": "red" });
        $(passive1).css({ "opacity": "0.1" });
        $(passive2).css({ "opacity": "0.1" });
    }

    function resetBallLocation() {
        CSS.ball.top = 300;
        CSS.ball.left = 443.5;
        $('#pong-ball').css('top', CSS.ball.top);
        $('#pong-ball').css('left', CSS.ball.left);
        $('#pong-ball-trail').css('top', CSS.ball.top);
        $('#pong-ball-trail').css('left', CSS.ball.left);
        $('#pong-ball-trail2').css('top', CSS.ball.top);
        $('#pong-ball-trail2').css('left', CSS.ball.left);
        $('#pong-ball-trail3').css('top', CSS.ball.top);
        $('#pong-ball-trail3').css('left', CSS.ball.left);
    }

    function resetButtonStyle() {
        $("#2-Players").css({ "opacity": "1", "color": "black" });
        $("#Player-vs-CPU").css({ "opacity": "1", "color": "black" });
        $("#CPU-vs-CPU").css({ "opacity": "1", "color": "black" });
    }

    function setEvents() {

        bindToMenuButtonClicks();

        $(document).on('keydown', function (e) {
            e.preventDefault();
            if(!CONSTS.isCpu2Active){
                if (e.keyCode === 87) {
                    CONSTS.stick1Speed = -5;
                } else if (e.keyCode === 83) {
                    CONSTS.stick1Speed = 5;
                } else if (e.keyCode === 38 && !CONSTS.isCpu1Active) {
                    CONSTS.stick2Speed = -5;
                } else if (e.keyCode === 40 && !CONSTS.isCpu1Active) {
                    CONSTS.stick2Speed = 5;
                }
            }
        });

        $(document).on('keyup', function (e) {
            if (e.keyCode === 87) {
                CONSTS.stick1Speed = 0;
            } else if (e.keyCode === 83) {
                CONSTS.stick1Speed = 0;
            } else if (e.keyCode === 38) {
                CONSTS.stick2Speed = 0;
            } else if (e.keyCode === 40) {
                CONSTS.stick2Speed = 0;
            }
        });
    }

    function isStickInBoundsOfArena(stickname) {
        return CSS[stickname].top + CSS.stick.height + CONSTS[stickname + "Speed"] <= CSS.arena.height && CSS[stickname].top + CONSTS[stickname + "Speed"] >= 0;
    }

    function createBallTrailEffect() {
        $('#pong-ball-trail').css({
            left: CSS.ball.left - CONSTS.ballLeftSpeed,
            top: CSS.ball.top - CONSTS.ballTopSpeed
        });

        $('#pong-ball-trail2').css({
            left: CSS.ball.left - CONSTS.ballLeftSpeed * 1.5,
            top: CSS.ball.top - CONSTS.ballTopSpeed * 1.5
        });

        $('#pong-ball-trail3').css({
            left: CSS.ball.left - CONSTS.ballLeftSpeed * 3,
            top: CSS.ball.top - CONSTS.ballTopSpeed * 3
        });
    }

    function changeScoreState(scoreElId, alertMessage) {
        var score = parseInt($(scoreElId).text(), 10);
        var nextScore = score + 1;
        $(scoreElId).text(nextScore);
        if (nextScore === 5) {
            CONSTS.isGameActive = false;
            setTimeout(() => {
                alert(alertMessage);
                reset();
            }, 10);
            reset();
            resetBallLocation();
        } else {
            roll();
        }
    }

    function loop() {
        window.pongLoop = setInterval(function () {
            if (CONSTS.isGameActive) {
                if (isStickInBoundsOfArena("stick1")) {
                    CSS.stick1.top += CONSTS.stick1Speed;
                    $('#stick-1').css('top', CSS.stick1.top);
                }
                if (isStickInBoundsOfArena("stick2")) {
                    CSS.stick2.top += CONSTS.stick2Speed;
                    $('#stick-2').css('top', CSS.stick2.top);
                }

                createBallTrailEffect();

                // Set ball next location
                CSS.ball.top += CONSTS.ballTopSpeed;
                CSS.ball.left += CONSTS.ballLeftSpeed;

                // When ball hit top of the screen or bottom of the screen the ball will be reflected like a mirror.
                if (CSS.ball.top <= 0 ||
                    CSS.ball.top >= CSS.arena.height - CSS.ball.height) {
                    CONSTS.ballTopSpeed = CONSTS.ballTopSpeed * -1;
                }

                // Ball move
                $('#pong-ball').css({ top: CSS.ball.top, left: CSS.ball.left });

                // When ball hit stick1
                if (CSS.ball.left <= CSS.stick.width) {
                    if (CSS.ball.top > CSS.stick1.top && CSS.ball.top < CSS.stick1.top + CSS.stick.height) {
                        (CONSTS.ballLeftSpeed = CONSTS.ballLeftSpeed * -1)
                    } else {
                        changeScoreState("#scoreRight", '2.oyuncu kazandı');
                    }
                }
                // When ball hit stick2
                else if (CSS.ball.left + 15 >= CSS.arena.width - 12 && CSS.ball.top >= CSS.stick2.top && CSS.ball.top <= CSS.stick2.top + 85) {
                    (CONSTS.ballLeftSpeed = CONSTS.ballLeftSpeed * -1)
                } else if (CSS.ball.left >= CSS.arena.width - CSS.ball.width - CSS.stick.width) {
                    changeScoreState("#scoreLeft", '1.oyuncu kazandı');
                }

                saveScoresToLocalStorage($('#scoreLeft').text(), $('#scoreRight').text());

                if (CONSTS.isCpu1Active) {
                    //  CPU
                    if (CSS.stick2.top > CSS.ball.top - (CSS.stick.height / 2)) {
                        CSS.stick2.top -= CONSTS.cpu1Speed;
                    }
                    if (CSS.stick2.top < CSS.ball.top - (CSS.stick.height / 2)) {
                        CSS.stick2.top += CONSTS.cpu1Speed;
                    }
                }

                if (CONSTS.isCpu2Active) {
                    if (CSS.stick1.top > CSS.ball.top - (CSS.stick.height / 2)) {
                        CSS.stick1.top -= CONSTS.cpu1Speed;
                    }
                    if (CSS.stick1.top < CSS.ball.top - (CSS.stick.height / 2)) {
                        CSS.stick1.top += CONSTS.cpu1Speed;
                    }
                }
            }

        }, CONSTS.gameSpeed);
    }

    function reset() {
        roll();
        $('#stick-1').css('top', CSS.stickinitial.top);
        $('#stick-2').css('top', CSS.stickinitial.top);
        CSS.stick1.top = CSS.stickinitial.top;
        CSS.stick2.top = CSS.stickinitial.top;
        $('#scoreLeft').text('0');
        $('#scoreRight').text('0');
    }

    // Ball Location and Movement
    function roll() {
        CSS.ball.top = 300;
        CSS.ball.left = 443.5;

        var side = -1;

        if (Math.random() < 0.5) {
            side = 1;
        }

        var deg = Math.random() * 120 + 30;
        var speed = 500;
        var rads = side * (deg * Math.PI / 180);
        CONSTS.ballLeftSpeed = Math.cos(rads) * speed / 60;
        CONSTS.ballTopSpeed = Math.sin(rads) * speed / 60;
    }
    start();
})();